#include <stdlib.h>
#include <stdio.h>
#include "gerrores.h"

error* addError(int type, int line, error *e) {
    if (e == NULL) {
        e = (error*) malloc(sizeof (error));
        e->line = line;
        switch (type) {
            case 0:
                e->trace = "Caracter no válido";
                break;
            case 1:
                e->trace = "Overflow"; //no sale
                break;
            case 2:
                e->trace = "Lexema incompleto";
                break;
            case 3:
                e->trace = "Error en reserva de memoria";
                break;
            case 4:
                e->trace = "Error al abrir archivo";
                break;
        }
    } else {
        e->next = addError(type, line, e->next);
    }
    return e;
}

void printList(error *e) {
    if (e == NULL)
        return;
    else {
        printf("%s in line %d\n", e->trace, e->line);
        printList(e->next);
    }
}