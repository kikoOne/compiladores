/* 
 * File:   sintactico.c
 * Author: J.Guzmán
 *
 * Created on 28 de septiembre de 2015, 13:54
 */

#include <stdio.h>
#include <stdlib.h>
#include "lexico.h"


int main(int argc, char** argv) {
    componentStruct* lexem; 
    node *root;
    init();
    do {
            lexem = nextLex();
        printf("\nLEXEMA: %s\nIDENTIFICADOR: %d \nNUMERO LÍNEA: %d \n------------\n", lexem->component, lexem->identifier, lexem->line);
        //    printf("%s ", lexem->component);
    } while ((lexem->identifier) != -666);
    printf("\n\n------Tabla de símbolos-------\n");
    root = get_avl();
    display_avl(root);
    printf("\n------------Lista de errores-----------\n");
    printErrList();
    return (EXIT_SUCCESS);
}

