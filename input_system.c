#include <stdio.h>
#include <stdlib.h>
#include <math.h>
//#include "input_system.h"
//#include "lexico.h"

#define N 128
#define SIZE 2*N+2

FILE* f = NULL;
char buffer[SIZE];
int start = 0, end = -1, pos, bf = 0, ov = 0;
//char* lexem;

void loadBuffer1() {
    int eof = fread(&buffer, sizeof (char), N, f);
    if (eof != N)
        buffer[eof] = EOF;
}

void loadBuffer2() {
    int eof = fread(&(buffer[N + 1]), sizeof (char), N, f);
    if (eof != N)
        buffer[eof + N] = EOF;
}

void initInputSystem(char *fname) {
    if (fname == NULL)
        fname = "wilcoxon.py";
    f = fopen(fname, "r");
    if (f == NULL)
        addE(4); // ????
    else {
        buffer[N] = EOF;
        buffer[SIZE - 1] = EOF;
        loadBuffer1();
    }
}

char getNextChar() {
    end++;
    pos = end % ((int) SIZE);
    if (end - start == N) {
        ov = 1;
    }
    if (buffer[pos] == EOF) {
        if (bf == 0 || ov == 1) {
            if (pos == N) {
                loadBuffer2();
                bf = 1;
            } else if (pos == SIZE - 1) {
                loadBuffer1();
                bf = 1;
            } else
                return EOF;
        }
        end++;
        pos = end % ((int) SIZE);
    }
    return buffer[pos];
}

void back() {
    end--;
    if (bf == 1 && buffer[end % ((int) SIZE)] == EOF)
        end--;
}

void ignore() {
    start = end;
    back();
    if (bf == 1)
        bf = 0;
    if (ov == 1)
        ov = 0;
}

char* getFullLexem(char *lexem) {
    int i = 0, loc, size;
    
    if (ov == 1) {
        addE(1);
        start = end - N + 1;
        ov = 0;
    }
    size = end - start + 2;
    
    if (lexem != NULL) {
        //free(lexem);
        lexem = (char*) malloc(size * sizeof (char));
        //lexem = (char*) realloc(lexem, size * sizeof (char));
    } else
        lexem = (char*) malloc(size * sizeof (char));

    while (start <= end) {
        loc = start % ((int) SIZE);
        if (loc != N && loc != SIZE - 1) {
            lexem[i] = buffer[loc];
            i++;
        } else {
            bf = 0;
        }
        start++;
    }
    lexem[size - 1] = '\0';
    return lexem;
}
