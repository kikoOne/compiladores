
typedef int bool;
#define true 1;
#define false 0;

//Palabras reservadas
#define IMPORT 300;
#define AS 301;
#define DEF 302;
#define FOR 303;
#define IN 304;
#define IF 305;
#define ELIF 306;
#define ELSE 307;
#define RETURN 308;
#define NOT 309;
#define PRINT 310;

//OTROS LEXEMAS: MACRO; CODIGO, EJEMPLO
#define INTEGER 400 // 230
#define FLOAT 401 // 2.  .23  2e3 2e+2 2e-2
#define HEXA 402 // 0x23
#define STRING 403 // "hola"  'hola'
#define OP_IGUAL 404 // ==
#define EXP 405 // **
#define INDENTATION 406
#define OP_INCREASE 407 // +=
#define MAJOR_EQUAL 408 // >=
#define MINOR_EQUAL 409 // <=

#define ID 500;

typedef struct{
    char* component;
    int identifier;
    int line;
} componentStruct;

typedef struct node {
    componentStruct* data;
    struct node* left;
    struct node* right;
    int height;
} node;

componentStruct* find(char* e, node* t);
node* insert(componentStruct* data, node *t);
void display_avl(node* t);
node* makeTable(node* t);
void freeTable();
void reset();