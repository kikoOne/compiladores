/* 
 * File:   lexico.h
 * Author: J.Guzmán
 *
 * Created on 28 de septiembre de 2015, 13:54
 */
#include "simbolos.h"

componentStruct* nextLex(); //analiza el siguiente componente lexico
node* init();
void close();
node* get_avl();
void addE(int t);
void printErrList();