
typedef struct{
    char* trace;
    int line;
    struct error *next;
} error;

error* addError(int type, int line, error *e);
void printList(error *e);