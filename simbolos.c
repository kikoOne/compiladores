#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "simbolos.h"

typedef int bool;
#define true 1;
#define false 0;

int count = 0;

componentStruct* new(componentStruct* e) {
    e = (componentStruct*) malloc(sizeof (componentStruct));
    return e;
}

node* makeTable(node* t) {
    componentStruct* e;
    e = new(e);
    e->component = "import";
    e->identifier = IMPORT;
    t = insert(e, t);

    e = new(e);
    e->component = "as";
    e->identifier = AS;
    t = insert(e, t);

    e = new(e);
    e->component = "def";
    e->identifier = DEF;
    t = insert(e, t);

    e = new(e);
    e->component = "for";
    e->identifier = FOR;
    t = insert(e, t);

    e = new(e);
    e->component = "in";
    e->identifier = IN;
    t = insert(e, t);

    e = new(e);
    e->component = "if";
    e->identifier = IF;
    t = insert(e, t);

    e = new(e);
    e->component = "elif";
    e->identifier = ELIF;
    t = insert(e, t);

    e = new(e);
    e->component = "else";
    e->identifier = ELSE;
    t = insert(e, t);

    e = new(e);
    e->component = "return";
    e->identifier = RETURN;
    t = insert(e, t);

    e = new(e);
    e->component = "not";
    e->identifier = NOT;
    t = insert(e, t);

    e = new(e);
    e->component = "print";
    e->identifier = PRINT;
    t = insert(e, t);
    return t;
}

void dispose(node* t) { //borrar todo el arbol
    if (t != NULL) {
        dispose(t->left);
        dispose(t->right);
        free(t);
    }
}

componentStruct* find(char* e, node* t) { //buscar un nodo
    if (t == NULL)
        return NULL;
    if (strcmp(e, (t->data)->component) < 0) // (e < (t->data)->component)
        return find(e, t->left);
    else if (strcmp(e, (t->data)->component) > 0) // (e > (t->data)->component)
        return find(e, t->right);
    else
        return t->data;
}

/*
    get the height of a node
 */
static int height(node* n) {
    if (n == NULL)
        return -1;
    else
        return n->height;
}

/*
    get maximum value of two integers
 */
static int max(int l, int r) {
    return l > r ? l : r;
}

/*
    perform a rotation between a k2 node and its left child
 
    note: call single_rotate_with_left only if k2 node has a left child
 */
static node* single_rotate_with_left(node* k2) {
    node* k1 = NULL;

    k1 = k2->left;
    k2->left = k1->right;
    k1->right = k2;

    k2->height = max(height(k2->left), height(k2->right)) + 1;
    k1->height = max(height(k1->left), k2->height) + 1;

    return k1; /* new root */
}

/*
    perform a rotation between a node (k1) and its right child
 
    note: call single_rotate_with_right only if
    the k1 node has a right child
 */
static node* single_rotate_with_right(node* k1) {
    node* k2;

    k2 = k1->right;
    k1->right = k2->left;
    k2->left = k1;

    k1->height = max(height(k1->left), height(k1->right)) + 1;
    k2->height = max(height(k2->right), k1->height) + 1;

    return k2; /* New root */
}

/*
 
    perform the left-right double rotation,
 
    note: call double_rotate_with_left only if k3 node has
    a left child and k3's left child has a right child
 */
static node* double_rotate_with_left(node* k3) {
    /* Rotate between k1 and k2 */
    k3->left = single_rotate_with_right(k3->left);

    /* Rotate between K3 and k2 */
    return single_rotate_with_left(k3);
}

/*
    perform the right-left double rotation
 
   notes: call double_rotate_with_right only if k1 has a
   right child and k1's right child has a left child
 */
static node* double_rotate_with_right(node* k1) {
    /* rotate between K3 and k2 */
    k1->right = single_rotate_with_left(k1->right);

    /* rotate between k1 and k2 */
    return single_rotate_with_right(k1);
}

node* insert(componentStruct* e, node* t) {
    if (t == NULL) {
        /* Create and return a one-node tree */
        t = (node*) malloc(sizeof (node));
        if (t == NULL) {
            //printf(stderr, "Out of memory!!! (insert)\n");
            //exit(1);
        } else {
            t->data = e;
            t->height = 0;
            t->left = t->right = NULL;
        }
    } else if (strcmp(e->component, ((t->data)->component)) < 0) { // (e->component < ((t->data)->component)) {
        t->left = insert(e, t->left);
        if (height(t->left) - height(t->right) == 2)
            if (strcmp(e->component, (t->left->data)->component) < 0) // (e->component < (t->left->data)->component)
                t = single_rotate_with_left(t);
            else
                t = double_rotate_with_left(t);
    } else if (strcmp(e->component, (t->data)->component) > 0) {// (e->component > (t->data)->component) {
        t->right = insert(e, t->right);
        if (height(t->right) - height(t->left) == 2)
            if (strcmp(e->component, (t->right->data->component)) > 0)//(e->component > (t->right->data)->component)
                t = single_rotate_with_right(t);
            else
                t = double_rotate_with_right(t);
    }
    /* Else X is in the tree already; we'll do nothing */

    t->height = max(height(t->left), height(t->right)) + 1;

    return t;
}

void display_avl(node* t) {
    if (t == NULL || t->data == NULL || t->data->component == NULL) {
        //printf("%d\n", count);
        return;
    }
    display_avl(t->left);

    printf("Lexema: %s        Línea: %d    Código:  %d\n", (t->data)->component, (t->data)->line, (t->data)->identifier);
    //printf("%s\n", (t->data)->component);
    count++;


    display_avl(t->right);
}

void reset() {
    count = 0;
}