#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "simbolos.h"
#include "input_system.h"
#include "gerrores.h"
//#include "lexico.h"

char element; // char actual a evaluar
int state = 0; // -1 ==> estado final ==> sale del compilador
int numLine = 1;
bool beginLine = true;
bool flag = false;
componentStruct *lex, *id;
node* tree = NULL;
char* temp;
error *e;

void printErrList() {
    printList(e);
}

void addE(int t) {
    e = addError(t, numLine, e);
}

void next() {
    element = getNextChar();
}

node* init() {
    initInputSystem(NULL);
    if (tree == NULL)
        tree = makeTable(tree);
    lex = (componentStruct*) malloc(sizeof (componentStruct));
    lex->component = NULL;
    return tree;
}

node* get_avl() {
    return tree;
}

void newComponent() {
    id = (componentStruct*) malloc(sizeof (componentStruct));
    id->line = numLine;
    id->identifier = ID;
    if (id->component == NULL)
        id->component = (char*) malloc(1);
    strcpy(id->component, temp);
}

void setData(componentStruct* comp, int res, char* src, char res2) {
    if (comp != NULL) {
        if (src == NULL)
            comp->component = getFullLexem(temp);
        else
            comp->component = src;

        comp->line = numLine;

        switch (res) {
            case -1:
                comp->identifier = res2;
                break;
            case 0:
                comp->identifier = INTEGER;
                break;
            case 1:
                comp->identifier = FLOAT;
                break;
            case 2:
                comp->identifier = HEXA;
                break;
            case 3:
                comp->identifier = STRING;
                break;
            case 4:
                comp->identifier = OP_IGUAL;
                break;
            case 5:
                comp->identifier = INDENTATION;
                break;
            case 6:
                comp->identifier = EXP;
                break;
            case 7:
                comp->identifier = OP_INCREASE;
                break;
            case 8:
                comp->identifier = MAJOR_EQUAL;
                break;
            case 9:
                comp->identifier = MINOR_EQUAL;
                break;
            case -666:
                comp->identifier = -666;
                break;
        }
    }
}

void checkNumE() { // 4
    next();
    if (!isdigit(element)) {
        state = -1; //END OF NUMBER
        back();
        setData(lex, 1, NULL, NULL);
    }
}

void checkFloat() { // 3
    next();
    if (!isdigit(element)) {
        state = -1;
        back();
        setData(lex, 1, NULL, NULL);
    }
}

void checkNumber() { // 2
    next();
    if (element == '.') {
        state = 3;
    } else if (element == 'e' || element == 'E') {
        next();
        if (element != '-' && element != '+' && !isdigit(element)) {
            back();
            back();
            state = -1;
            setData(lex, 0, NULL, NULL);
        } else if (element == '-' || element == '+') {
            next();
            if (!isdigit(element)) {
                back();
                back();
                back();
                state = -1;
                setData(lex, 0, NULL, NULL);
            } else {
                back();
                state = 4;
            }
        } else
            state = 4;
    } else if (!isdigit(element)) {
        state = -1;
        back();
        setData(lex, 0, NULL, NULL);
    }
}

bool ishexa(char c) {
    if (isdigit(c) || (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F'))
        return true;
    return false;
}

void checkHexa() {
    next();
    if (element == 'x') {
        state = 6;
        next();
        if (!ishexa(element)) {
            back();
            back();
            state = -1;
            setData(lex, 0, NULL, NULL);
        } else
            back();
    } else {
        back();
        state = 2; //check number
    }
}

void checkHexaDigits() {
    next();
    if (!ishexa(element)) {
        back();
        state = -1;
        setData(lex, 2, NULL, NULL);
    }
}

void checkId() { // 1
    next();
    if (!isalpha(element) && !isdigit(element) && !(element == '_')) {
        state = -1;
        flag = true;
        back();
        temp = getFullLexem(temp);
        id = find(temp, tree);
        if (id == NULL) {
            newComponent();
            tree = insert(id, tree);
        } else if (id->identifier == 500) {
            id->line = numLine;
        }
    }
}

void checkVoidConstant() { // 12
    next();
    if (element == '"') //segunda comilla
        state = 14;
    else
        state = 13;
}

void checkString() { // 13
    next();
    if (element == EOF || element == '\n') {
        back();
        addE(2);
        state = -1;
        setData(lex, 3, NULL, NULL);
    } else if (element == '"') { // cierre comillas
        state = -1;
        setData(lex, 3, NULL, NULL);
    }
}

void checkMultiline() { //14
    next();
    if (element == '"')
        state = 20;
    else { // return void constant
        state = -1;
        setData(lex, 3, NULL, NULL);
    }
}

int isErrMultiline() {
    if (element == EOF) {
        return 1;
    } else
        return 0;
}

void checkInsideMultiline() { // 20
    //next();
    int count = 0;
    while (count != 3) {
        next();
        if (isErrMultiline()) {
            //GESTIONAR ERROR
            addE(2);
            back();
            count = 3;
            state = -1;
            setData(lex, 3, NULL, NULL);
        } else if (element == '\n') {
            numLine++;
        } else if (element == '"') {
            count++;
            if (count == 3) {
                state = -1;
                setData(lex, 3, NULL, NULL);
            }
        } else {
            count = 0;
        }
    }
}

void checkComment() {
    next();
    state = 11;
    if (element == '\n') {
        if (beginLine == 1) {
            next();
            numLine++;
        }
        ignore();
        state = 0;
    }
}

void checkAsign() {
    next();
    if (element == '=') {
        state = -1;
        setData(lex, 4, NULL, NULL);
    } else {
        back();
        state = -1;
        setData(lex, -1, NULL, '=');
    }
}

void checkJump() {
    numLine++;
    next();
    if (element != '\n') {
        beginLine = true;
        ignore();
        state = -1;
        setData(lex, -1, "\n", '\n');
    }
}

void checkSpace() {
    next();
    if (element != ' ') {
        if (beginLine == 0) {
            state = 0;
            ignore();
        } else if (beginLine == 1 && element == '#') {
            state = 0;
            ignore();
        } else {
            beginLine = false;
            state = -1;
            back();
            setData(lex, 5, NULL, NULL);
        }
    }
}

void checkDot() {
    next();
    if (!isdigit(element)) {
        state = -1;
        back();
        setData(lex, -1, NULL, '.');
    } else {
        state = 3;
    }
}

void checkOp() {
    if (element == '*') {
        next();
        if (element == '*')
            setData(lex, 6, NULL, NULL);
        else {
            back();
            setData(lex, -1, NULL, '*');
        }
        state = -1;
    } else if (element == '+') {
        next();
        if (element == '=')
            setData(lex, 7, NULL, NULL);
        else {
            back();
            setData(lex, -1, NULL, '+');
        }
        state = -1;
    } else if (element == '-') {
        setData(lex, -1, NULL, '-');
        state = -1;
    } else if (element == '/') {
        setData(lex, -1, NULL, '/');
        state = -1;
    }
}

void checkConstVoid2() {
    next();
    if (element == EOF || element == '\n') {
        addE(2);
        back();
        setData(lex, 3, NULL, NULL);
        state = -1;
    } else if (element == '\'') {
        setData(lex, 3, NULL, NULL);
        state = -1;
    }
}

void checkEq() {
    if (element == '<') {
        next();
        if (element != '=') {
            back();
            setData(lex, -1, NULL, '<');
        } else
            setData(lex, 9, NULL, NULL);
    } else if (element == '>') {
        next();
        if (element != '=') {
            back();
            setData(lex, -1, NULL, '>');
        } else
            setData(lex, 8, NULL, NULL);
    }
    state = -1;
}

void checkAscii() {
    switch (element) {
        case '=':
            state = 8;
            break;
        case'-': case '+': case '*': case'/':
            state = 9;
            break;
        case '<': case '>':
            state = 10;
            break;
        case '#':
            state = 11;
            break;
        case '"':
            state = 12;
            break;
        case '\'':
            state = 18;
            break;
        case '\n':
            state = 15;
            break;
        case ' ':
            state = 16;
            break;
        case '.':
            state = 17;
            break;
        case'(': case'[': case '{': case')': case']': case '}': case':': case',':
            state = -1;
            setData(lex, -1, NULL, element); 
            break;
        case '_':
            state = 1;
            break;
        default:
            addE(0);
            next();
            ignore();
            break;
    }
}

void checkFirstElement() {
    next();
    if (beginLine == 1 && element != ' ' && element != '#')
        beginLine = false;
    if (isalpha(element)) { //si alphabetico
        state = 1;
    } else if (isdigit(element)) { //es digicto
        if (element == '0')
            state = 5;
        else
            state = 2;
    } else if (isascii(element)) { //es ascii        
        checkAscii();
    } else if (element == EOF) {
        state = -1;
        setData(lex, -666, "", NULL);
    }
}

componentStruct * nextLex() {
    state = 0;
    while (state > -1) {
        switch (state) {
            case 0:
                checkFirstElement();
                break;
            case 1:
                checkId();
                break;
            case 2:
                checkNumber();
                break;
            case 3:
                checkFloat();
                break;
            case 4:
                checkNumE();
                break;
            case 5:
                checkHexa();
                break;
            case 6:
                checkHexaDigits();
                break;
            case 8:
                checkAsign();
                break;
            case 9:
                checkOp();
                break;
            case 10:
                checkEq();
                break;
            case 11:
                checkComment();
                break;
            case 12:
                checkVoidConstant();
                break;
            case 13:
                checkString();
                break;
            case 14:
                checkMultiline();
                break;
            case 15:
                checkJump();
                break;
            case 16:
                checkSpace();
                break;
            case 17:
                checkDot();
                break;
            case 18:
                checkConstVoid2();
                break;
            case 20:
                checkInsideMultiline();
                break;
        }
    }

    if (!flag)
        return lex;
    else {
        flag = false;
        return id;
    }
}
